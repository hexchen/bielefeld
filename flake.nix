{
  description = "bielefeld: the kubernetes on nixos doesn't exist";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.disko.url = "github:nix-community/disko";
  inputs.disko.inputs.nixpkgs.follows = "/nixpkgs";
  inputs.colmena.url = "github:zhaofengli/colmena/main";
  inputs.colmena.inputs.nixpkgs.follows = "/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.sops-nix.url = "github:Mic92/sops-nix";
  inputs.sops-nix.inputs.nixpkgs.follows = "/nixpkgs";
  inputs.home-manager.url = "github:nix-community/home-manager";
  inputs.home-manager.inputs.nixpkgs.follows = "/nixpkgs";
  inputs.nixos-anywhere.url = "github:numtide/nixos-anywhere";
  inputs.nixos-anywhere.inputs.nixpkgs.follows = "/nixpkgs";

  outputs = { self, nixpkgs, colmena, flake-utils, sops-nix, home-manager, nixos-anywhere, ...
    }@inputs:
    let
      lib = nixpkgs.lib;

      cluster = import ./cluster.nix;
      specialArgs = {
        inherit inputs;
        inherit hosts;
      };
      colmenaHiveMeta = {
        allowApplyAll = true;
        description = "bielek8s";
        machinesFile = null;
        name = "hive";
      };
      clusterHost = name: profiles: rec {
        target = cluster.targets.${name} or "${name}.bi.cuties.network";
        nixosConfiguration = nixpkgs.lib.nixosSystem {
          inherit specialArgs;
          system = "x86_64-linux";
          modules = lib.flatten [
            (cluster.hostTypes.magic { inherit name target; })
            cluster.hostTypes.default
            profiles
          ];
        };
      };
      clusterHosts = lib.mapAttrs clusterHost cluster.hosts;
      hosts = {
        bootstrap = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs; };
          modules = [
            ./nix/hosts/bootstrap/config.nix
          ];
        };
      } // lib.mapAttrs (_: host: host.nixosConfiguration) clusterHosts;
    in {
      colmenaHive = import ./nix/lib/colmena-compat.nix { inherit nixpkgs colmena colmenaHiveMeta; hosts = clusterHosts; };

      nixosConfigurations = hosts;
      diskoConfigurations.bootstrap = import ./nix/hosts/bootstrap/disk.nix;
      overlays = {};
    } // flake-utils.lib.eachSystem ([
      "x86_64-linux"
      "x86_64-darwin"
      "aarch64-linux"
      "aarch64-darwin"
    ]) (system:
    let
      pkgs = (import nixpkgs {
        inherit system;
        overlays = builtins.attrValues inputs.self.overlays;
      });
    in {
        legacyPackages = pkgs;
        devShells.default = pkgs.mkShell {
          buildInputs = [
            colmena.packages.${system}.colmena
            sops-nix.packages.${system}.sops-init-gpg-key
            nixos-anywhere.packages.${system}.default
            pkgs.age
            pkgs.sops
            pkgs.cachix
          ];
        };
      });
}
