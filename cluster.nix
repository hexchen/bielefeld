rec {
  hostTypes = {
    default = [ ./nix/profiles/base ];
    magic = import ./nix/profiles/magic;

    hardware.BL460cG7 = [ ./nix/hardware/BL460cG7.nix ];

    roles.controlplane = [ ./nix/roles/controlplane ];
    roles.worker = [ ./nix/roles/worker ];
  };

  hosts = {
    control1 = with hostTypes; [ hardware.BL460cG7 roles.controlplane ];
    control2 = with hostTypes; [ hardware.BL460cG7 roles.controlplane ];
    control3 = with hostTypes; [ hardware.BL460cG7 roles.controlplane ];
    worker1  = with hostTypes; [ hardware.BL460cG7 roles.worker ];
    worker2  = with hostTypes; [ hardware.BL460cG7 roles.worker ];
    worker3  = with hostTypes; [ hardware.BL460cG7 roles.worker ];

    spare1   = with hostTypes; [ hardware.BL460cG7 ];
  };

  targets = {
    control1 = "192.168.50.133";
    control2 = "192.168.50.146";
    control3 = "192.168.50.153";
    worker1 =  "192.168.50.173";
    worker2 =  "192.168.50.147";
    worker3 =  "192.168.50.160";
    spare1 =   "192.168.50.141";
  };
}
