{pkgs, modulesPath, inputs, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    inputs.disko.nixosModules.disko
  ];

  disko.devices = (import ./disk.nix {}).disko.devices;

  boot.loader.grub.devices = [ "/dev/sda" ];
  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "be2iscsi" "hpsa" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
  boot.kernelModules = [ "kvm-intel" ];

  services.openssh.enable = true;

  networking.useDHCP = true;

  users.users.root.password = "bielek8s";
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINJ0tCxsEilAzV6LaNpUpcjzyEn4ptw8kFz3R+Z3YjEF hexchen@chimborazo"
  ];

  system.stateVersion = "23.05";
}
