{ config, lib, pkgs, ... }:

let
  path = with pkgs; [ cri-tools openssh docker utillinux iproute ethtool thin-provisioning-tools iptables socat conntrack-tools kubernetes-helm ];
in {
  boot.kernelModules = [ "br_netfilter" ];

  boot.kernel.sysctl = {
    # TODO IPV6/DualStack
    "net.ipv4.ip_forward" = 1;
    "net.bridge.bridge-nf-call-iptables" = 1;
  };

  environment.systemPackages = [ pkgs.kubernetes ] ++ path;

  virtualisation.containerd.enable = true;
  virtualisation.containerd.settings = {
    version = 2;
    root = "/var/lib/containerd";
    state = "/run/containerd";
    oom_score = 0;

    grpc = {
      address = "/run/containerd/containerd.sock";
    };

    plugins."io.containerd.grpc.v1.cri" = {
      sandbox_image = "registry.k8s.io/pause:3.9";

      cni = {
        bin_dir = "/opt/cni/bin";
        max_conf_num = 0;
      };

      containerd.runtimes.runc = {
        runtime_type = "io.containerd.runc.v2";
        options.SystemdCgroup = true;
      };
    };
  };

  systemd.services.kubelet = {
    description = "Kubernetes Kubelet Service";
    wantedBy = [ "multi-user.target" ];

    inherit path;

    unitConfig = {
      StartLimitInterval= 0;
    };
    serviceConfig = {
      StateDirectory = "kubelet";
      ConfigurationDirectory = "kubernetes";

      # This populates $KUBELET_KUBEADM_ARGS and is provided
      # by kubeadm init and join
      EnvironmentFile = "-/var/lib/kubelet/kubeadm-flags.env";

      Restart = "always";
      RestartSec = 10;

      ExecStart = ''
          ${pkgs.kubernetes}/bin/kubelet \
            --kubeconfig=/etc/kubernetes/kubelet.conf \
            --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf \
            --config=/var/lib/kubelet/config.yaml \
            $KUBELET_KUBEADM_ARGS
        '';
    };
  };
}
