{ nixpkgs, hosts, colmena, colmenaHiveMeta }:

with nixpkgs.lib;

let
  inherit (nixpkgs) lib;

  evalModule = config: evalModules {
    modules = [ colmena.nixosModules.deploymentOptions config ];
  };

  getDeploymentConfig = n: v: evalModule {
    deployment.allowLocalDeployment = false;
    deployment.targetHost = v.target;
    deployment.targetPort = 22;
    deployment.targetUser = "root";
  };
in rec {
  __schema = "v0";

  nodes = mapAttrs (_: v: v.nixosConfiguration) hosts;
  toplevel = mapAttrs (_: v: v.config.system.build.toplevel) nodes;
  deploymentConfig = mapAttrs (n: v: (getDeploymentConfig n v).config.deployment) hosts;
  deploymentConfigSelected = names: filterAttrs (name: _: elem name names) deploymentConfig;
  evalSelected = names: filterAttrs (name: _: elem name names) toplevel;
  evalSelectedDrvPaths = names: mapAttrs (_: v: v.drvPath) (evalSelected names);
  metaConfig = colmenaHiveMeta;
  introspect = f:
    f {
      inherit lib nodes hosts;
      pkgs = nixpkgs;
    };
}
