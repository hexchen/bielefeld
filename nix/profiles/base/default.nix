{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    inputs.disko.nixosModules.disko
  ];

  services.openssh.enable = true;

  networking.useDHCP = true;
  networking.domain = "bi.cuties.network";
  networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];
  # TODO: fix this
  networking.firewall.enable = false;

  # this is just a testing system!
  # TODO: remove this before going into prod!
  users.users.root.password = "bielek8s";
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINJ0tCxsEilAzV6LaNpUpcjzyEn4ptw8kFz3R+Z3YjEF hexchen@chimborazo"
  ];

  system.stateVersion = "23.05";
}
